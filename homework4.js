// Теоретичні питання
// Описати своїми словами навіщо потрібні функції у програмуванні.
// Функции необходимы для того, чтоб при внесении изменений в проекте не нужно было их прописывать в 100 мест, а с помощью функции изменения для 100 мест можно указать в одной лишь функции
// Описати своїми словами, навіщо у функцію передавати аргумент.
// Аргумент дают возможность гибко работать с функцией.
// Що таке оператор return та як він працює всередині функції?
// return завершает выполнение текущей функции и возвращает её значение.
// Завдання
// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.
// Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).
let firstNumber = prompt("enter first number");
let secondNumber = prompt("enter second number");
function getFirstNumber(){
    if(isNaN(+firstNumber) || firstNumber === null || firstNumber === "") {
        firstNumber = +prompt("enter first number:");
        }
    return firstNumber;    
}


function getSecondNumber() {
    if (isNaN(+secondNumber) || secondNumber === null || secondNumber === "") {
        secondNumber = +prompt("enter second number");
    }
    return secondNumber;
}

function getSpecialSymbol() {
let specialSymbol = prompt("enter special symbol: +, _, *, /");
if (!/^[\-+*(/)]/) {
    specialSymbol = prompt("enter special symbol: +, _, *, /");
}
return specialSymbol;

}

function calcNumber() {
    let mathSymbol = getSpecialSymbol ();
    // ------------------------------------не работает с if все кроме сложения-----------------------
    // if ( '+' ) {
    //     return  getFirstNumber() + getSecondNumber();
    // }
    // else if ( "-" ) {
    //     return  getFirstNumber() - getSecondNumber();
    // }
    // else if ( '*' ) {
    //     return  getFirstNumber() * getSecondNumber();
    // }
    // else if ( '/' ) {
    //     return  getFirstNumber() / getSecondNumber();
    // }
    // ------------------co switch работает---------------
    switch (mathSymbol) {
        case "+": {
            return  getFirstNumber() + getSecondNumber();
        }
        case "-":{
            return  getFirstNumber() - getSecondNumber();
        }
        case "/":{
            return  getFirstNumber() / getSecondNumber();
        }
        case "*":{
            return  getFirstNumber() * getSecondNumber();
        }

    }
  
}
console.log(calcNumber());
